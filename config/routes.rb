Korepetytor::Application.routes.draw do


 

  devise_for :users , :path => 'konto' #TODO: nazwy akcji 
     
  
  resources :users, :path => "uzytkownik",  :only => [] do
    resources :comments,  :path => "komentarze", :only => [:update, :edit, :index], :path_names => { :edit => "wystaw" } 
    resource :room,  :path => "pokoj"
    resource :tutor ,  :path => "korepetytor", :path_names => { :new => "zostan", :edit => "edytuj" } 
    resources :payment_histories, :only => [:create]
    resource :payment_management,  :path => "moje_platnosci", :only => [:show]
    resource :my_profile, :path => "moj_profil", :only => :show
  end
 
  resources :messages, :only => [:create]
  resources :contacts, :only => [:create]

  resources :tutors, :path => "korepetytor", :only => [:show] do
    resource :private_room, :path => "prywatna_lekcja" do
      member do
        get :annouce
      end
    end
    resources :comments,  :path => "komentarze",  :only => [:index]
    member do
      put :online
    end
    
  end
  
  resources :subjects, :path => "przedmioty", :only => [:index, :show] do
    resources :tutors, :path => "korepetytorzy", :only => [:index]
  end
  
  resources :payment_notifications, :only => :create

  resources :search_resoults, :path => "szukam", :only => [:index]

  resources :welcomes, :path => "jaktodziala", :only => [:index]

  root :to => 'subjects#index'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
