require 'faker'
namespace :db do
  namespace :pop do
    task tutors: :environment do
      desc "Tworzy przykladowe dane"
      #    Rake::Task['db:reset'].execute
      #    Rake::Task['db:test:prepare'].execute
      #    Rake::Task['import'].execute
    
      puts "Tworze Tutors"     
      make_tutors

    

    
      
    end
 
    task subjects: :environment do
      puts "Wybieram losowe przedmioty dla 15 korepetytorow:" 
      tutors = Tutor.all(limit: 15)
      make_tutor_subjects(tutors)
    end
    
    
    task online: :environment do
      puts "Ustawiam status online dla losowych korepetytorow:" 
      set_online_status
    end
 
    task comments: :environment do
      puts "Generuje komentarze dla losowych korepetytorow:" 

        
      generate_comments
    end
    
    
  end
end


def make_tutors
  User.delete_all
  Tutor.delete_all
  Room.delete_all
  15.times do |n|
    print "." 
    #tworzenie uzytkownika
    nicknameArray = %w[ Adanos Adelae Adik14 adriano1988 afro80 andrev96 anestBee
                        Argonath BadMojo banshior  bialymurzyn biegly2 bigandrew Binienda79
                        Blinkoo Bonjasky bonkil Brakus Brouli bufu48 carboN cavall Charlie CheGuevara
                        collo27 CooLOn CzaHa90 darurka dezetka dik565 dondan
    ]
                
    nickname = nicknameArray[n]
    email = "example#{n}@gmail.com"
    password = "123456"
    user = User.create!(nickname: nickname, email: email, password: password,
      password_confirmation: password)
    
    #tworzenie korepetytora
    name = Faker::Name.first_name
    surname = Faker::Name.last_name
    degree = "Podstawowe"
    about = Faker::Lorem.sentence(10)
    price = 1 + rand(3)
    short_description = Faker::Lorem.sentence(10)
    last_seen_at = Time.now
    user.create_tutor!(name: name, surname: surname, degree: degree, about: about, price: price, 
      short_description: short_description, last_seen_at: last_seen_at ) 
    
    
    #tworzenie pokoju
    sessionIdArray = %w[ 2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxNDozOSBQU1QgMjAxMn4wLjY1NjAyMTJ-
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxNzoxNyBQU1QgMjAxMn40Ljg0MjI4MTNFLTR-
                         1_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxNzozMiBQU1QgMjAxMn4wLjU1MjUyMTM1fg
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxNzo0MCBQU1QgMjAxMn4wLjYxMTA2MDF-
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxNzo0NiBQU1QgMjAxMn4wLjg0MzQwOTd-
                         1_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxNzo1OSBQU1QgMjAxMn4wLjc5NTA2M34
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxODowOCBQU1QgMjAxMn4wLjI3MzQ4MjM4fg
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxODoxNCBQU1QgMjAxMn4wLjczMDg0NzV-
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxODoyMiBQU1QgMjAxMn4wLjQzNTU0NzM1fg
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxODozNiBQU1QgMjAxMn4wLjc1MTc1MDM1fg
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxODo0NCBQU1QgMjAxMn4wLjU4NzY5Nzh-
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxODo1NyBQU1QgMjAxMn4wLjA2NTQ4Mzg3fg
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxOTowNiBQU1QgMjAxMn4wLjMyNTY1MDQ1fg
                         1_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxOToyMSBQU1QgMjAxMn4wLjgwMzc5ODF-
                         1_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxOToyOCBQU1QgMjAxMn4wLjA1NDI3Mjc3fg
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxOTozMyBQU1QgMjAxMn4wLjY0MDc4ODN-
                         2_MX4yMTE4NzE5Mn5-TW9uIERlYyAxNyAwODoxOTozOSBQU1QgMjAxMn4wLjc1NDI0MTN-
    ]
    room_name = Faker::Name.title
    sessionId = sessionIdArray[n]
    user.create_room!(name: room_name, sessionId: sessionId ) 
    
  end
end

def uniq_subject
  array = []
  3.times do |n| 
    array << 2 + rand(5)
  end
  return array.uniq
end


def make_tutor_subjects(tutors)  
  tutors.each  do |tutor| 
    uniq_subject.each do |subject_id|
    tutor.subjects << Subject.find_by_id(subject_id)
    end
  end
  
end

def set_online_status
  (0..5).each do |n|  
    tutor = Tutor.offset(rand(Tutor.count)).first
    tutor.set_online_status!  
    puts "Korepetytor #{tutor.user.nickname} ONLINE!"     
  end
end


def generate_comments
  (0..20).each do |n|  
    tutor = Tutor.offset(rand(Tutor.count)).first
    user = User.offset(rand(User.count)).first  
    
    puts "Tworze komentarz od #{user.nickname} dla korepetytora #{tutor.user.nickname}!"  
    
    c = Comment.new
    c.tutor_id = tutor.id
    c.sender_id = user.id
    c.rate = 1 + rand(4)
    c.content = Faker::Lorem.sentence(10)
    
    c.save!
    
    tutor.new_comment_handler!(c.rate)
    
  end
  
  
end