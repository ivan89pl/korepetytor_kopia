require 'csv'

desc "Import Subject from csv file"
task :import => [:environment] do

  puts "Deleting all from Subjects table" 
  Subject.delete_all
  puts "Reseting primary key counting" 
  ActiveRecord::Base.connection.reset_pk_sequence!(:subjects)
 
  file = "db/subjects.csv"
  puts "Importing records from .csv"
  CSV.foreach(file, :headers => true) do |row|
    Subject.create( :name => row[0])#, :level => row[1] )
  end
  puts "Done"
end