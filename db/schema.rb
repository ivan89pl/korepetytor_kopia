# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121217114921) do

  create_table "comments", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "tutor_id"
    t.integer  "private_room_id"
    t.integer  "rate"
    t.text     "content"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "messages", :force => true do |t|
    t.text     "content"
    t.string   "sender"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "conversation_id"
    t.string   "conversation_type"
  end

  create_table "payment_histories", :force => true do |t|
    t.integer  "user_id"
    t.integer  "sender_id"
    t.integer  "token"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "payment_notifications", :force => true do |t|
    t.text     "params"
    t.string   "status"
    t.string   "transaction_id"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "private_rooms", :force => true do |t|
    t.string   "sessionId"
    t.time     "elapsed_time"
    t.integer  "learner_id"
    t.integer  "tutor_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.datetime "expire_in"
  end

  create_table "rooms", :force => true do |t|
    t.string   "name"
    t.string   "sessionId"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  create_table "subjects", :force => true do |t|
    t.string   "name"
    t.string   "level"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.integer  "tutor_subjects_count", :default => 0
  end

  create_table "tutor_subjects", :force => true do |t|
    t.integer  "subject_id"
    t.integer  "tutor_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tutors", :force => true do |t|
    t.string   "name"
    t.string   "surname"
    t.string   "degree"
    t.text     "about"
    t.boolean  "online",              :default => false
    t.integer  "user_id"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "price",               :default => 1
    t.integer  "comments_count",      :default => 0
    t.float    "rating"
    t.integer  "sort_order",          :default => 0
    t.text     "short_description"
    t.datetime "last_seen_at",        :default => '2012-12-31 23:00:00'
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "nickname"
    t.integer  "token",                  :default => 0
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["nickname"], :name => "index_users_on_nickname", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
