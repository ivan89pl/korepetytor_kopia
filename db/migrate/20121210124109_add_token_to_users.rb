class AddTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :token, :integer, :default => 0
  end
end
