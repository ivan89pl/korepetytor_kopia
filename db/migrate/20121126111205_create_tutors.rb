class CreateTutors < ActiveRecord::Migration
  def change
    create_table :tutors do |t|
      t.string :name
      t.string :surname
      t.string :degree
      t.text :about
      t.boolean :online, :default => false
      t.integer :user_id
      
      

      t.timestamps
    end
  end
end
