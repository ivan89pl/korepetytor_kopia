class CreateTutorSubjects < ActiveRecord::Migration
  def change
    create_table :tutor_subjects do |t|
      t.integer :subject_id
      t.integer :tutor_id
      t.timestamps
    end
  end
end
