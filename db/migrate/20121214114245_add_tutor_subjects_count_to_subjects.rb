class AddTutorSubjectsCountToSubjects < ActiveRecord::Migration
  def change
     add_column :subjects, :tutor_subjects_count, :integer, :default => 0
  end
end
