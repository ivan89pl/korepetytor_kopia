class CreatePrivateRooms < ActiveRecord::Migration
  def change
    create_table :private_rooms do |t|
      t.string :sessionId
      t.time :elapsed_time
      t.date :expire_in
      t.integer :learner_id
      t.integer :tutor_id

      t.timestamps
    end
  end
end
