class AddAttachmentAvatarToTutors < ActiveRecord::Migration
  def self.up
    change_table :tutors do |t|
      t.has_attached_file :avatar
    end
  end

  def self.down
    drop_attached_file :tutors, :avatar
  end
end
