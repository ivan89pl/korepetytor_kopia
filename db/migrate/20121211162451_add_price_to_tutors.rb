class AddPriceToTutors < ActiveRecord::Migration
  def change
    add_column :tutors, :price, :integer, :default => 1
  end
end
