class AddShortDescriptionToTutor < ActiveRecord::Migration
  def change
    add_column :tutors, :short_description, :text
  end
end
