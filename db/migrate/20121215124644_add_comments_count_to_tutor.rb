class AddCommentsCountToTutor < ActiveRecord::Migration
  def change
    add_column :tutors, :comments_count, :integer, :default => 0
  end
end
