class FixMessage < ActiveRecord::Migration
  def up
    remove_column :messages, :user_id
    add_column :messages, :sender, :string
  end

  def down
  end
end
