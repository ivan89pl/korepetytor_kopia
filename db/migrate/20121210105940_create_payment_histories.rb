class CreatePaymentHistories < ActiveRecord::Migration
  def change
    create_table :payment_histories do |t|
      t.integer :user_id
      t.integer :sender_id
      t.integer :token  
      t.timestamps
    end
  end
end
