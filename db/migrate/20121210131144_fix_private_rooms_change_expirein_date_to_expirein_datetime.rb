class FixPrivateRoomsChangeExpireinDateToExpireinDatetime < ActiveRecord::Migration
  def up
    remove_column :private_rooms, :expire_in
    add_column :private_rooms, :expire_in, :datetime
  end

  def down
    remove_column :private_rooms, :expire_in
    add_column :private_rooms, :expire_in, :date
  end
end
