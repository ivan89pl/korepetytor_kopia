class AddLastSeenAtToTutor < ActiveRecord::Migration
  def change
    add_column :tutors, :last_seen_at, :datetime, :default => Time.now.beginning_of_year
  end
end
