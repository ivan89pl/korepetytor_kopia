class FixMessageChangeToPolymorphicAssociation < ActiveRecord::Migration
  def up
    remove_column :messages, :room_id
    add_column :messages, :conversation_id, :integer
    add_column :messages, :conversation_type, :string
  end

  def down
    add_column :messages, :room_id, :integer
    remove_column :messages, :conversation_id
    remove_column :messages, :conversation_type   
  end
end
