class AddRatingAndSortOrderToTutor < ActiveRecord::Migration
  def change
    add_column :tutors, :rating, :float
    add_column :tutors, :sort_order, :integer, :default => 0
  end
end
