class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :sender_id
      t.integer :tutor_id
      t.integer :private_room_id      
      t.integer :rate
      t.text :content

      t.timestamps
    end
  end
end
