# == Schema Information
#
# Table name: private_rooms
#
#  id           :integer          not null, primary key
#  sessionId    :string(255)
#  elapsed_time :time
#  learner_id   :integer
#  tutor_id     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  expire_in    :datetime
#

require 'spec_helper'

describe PrivateRoom do
  
  let(:learner) { FactoryGirl.create(:user) }
  let(:tutor) { prepare_tutor}
  let(:private_room) {tutor.create_private_room(FactoryGirl.attributes_for(:private_room).merge(:learner_name => learner.nickname))}
  
  subject { private_room }
  it { should respond_to(:sessionId) } 
  it { should be_valid }
  

  
  describe "not accessible attributes" do
    it "deny access to tutor_id" do
      expect { PrivateRoom.new(tutor_id: "1") }.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
    it "deny access to learner_id" do
      expect { PrivateRoom.new(learner_id: "1") }.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end
  
  
end
