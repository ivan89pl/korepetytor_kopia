# == Schema Information
#
# Table name: rooms
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  sessionId  :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'spec_helper'

describe "Rooms" do
  
  let(:user) { FactoryGirl.create(:user) } 
  let(:room) { user.build_room(FactoryGirl.attributes_for(:room)) }   
  
  
  subject { room }
  it { should respond_to(:name) }
  it { should respond_to(:sessionId) } 
  it { should be_valid }
  

  
  describe "not accessible attributes" do
    it "not allow access to user_id" do
      expect { Room.new(user_id: "1") }.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end
  
  describe "validations" do  
    context "name cant be nil" do   
      before { room.name = nil}
      it { room.should_not be_valid }    
    end
    context "name cant be blank" do   
      before { room.name = " "}
      it { should_not be_valid }    
    end
    context "sessionId cant be nil" do   
      before { room.sessionId = nil}
      it { should_not be_valid }    
    end 
  end
end
