# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  nickname               :string(255)
#
require 'spec_helper'

describe "Users" do
 
  let(:user) { FactoryGirl.create(:user) }
  
  subject { user }
  
  it { should respond_to(:email)}
  it { should respond_to(:nickname)}
  it { should be_valid }
  
  
  describe "validations" do
    context "nil nickname" do
      before { user.nickname = nil }
      it { should_not be_valid }
    end
    context "empty nickname" do
      before { user.nickname = " " }
      it { should_not be_valid }
    end
    context "too short nickname" do
      before { user.nickname = "a" * 5  }
      it { should_not be_valid }
    end
    context "too long nickname" do
      before { user.nickname = "a" * 21 }
      it { should_not be_valid }
    end  
  end 
  
  describe "methods" do
    describe ".tutor?" do
      it "user isn't a tutor" do                
        expect(user.tutor?).to be_false                                  
      end
      it "user is a tutor" do
        user.create_tutor(FactoryGirl.attributes_for(:tutor)) 
        user.create_room(FactoryGirl.attributes_for(:room))              
        expect(user.tutor?).to be_true                                   
      end
    end
    describe ".enough_tokens?" do
      
    end
    
  end
  
end
