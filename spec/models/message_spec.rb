# == Schema Information
#
# Table name: messages
#
#  id                :integer          not null, primary key
#  content           :text
#  sender            :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  conversation_id   :integer
#  conversation_type :string(255)
#

# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  content    :text
#  room_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
    
require 'spec_helper'

describe Message do
  
  let(:user)    { FactoryGirl.create(:user) }
  let(:room)    { user.create_room(FactoryGirl.attributes_for(:room))}
  let(:message) { room.messages.new(FactoryGirl.attributes_for(:message)) }

  
  subject { message }
  it { should respond_to(:content) }
  it { should respond_to(:sender) }
  it { should be_valid }

  

  
  describe "validations" do
    context "reject nil content" do   
      before { message.content = nil}
      it { should_not be_valid }    
    end
    context "reject blank content" do   
      before { message.content = " "}
      it { should_not be_valid }    
    end
    
    context "should reject too long content (max 120 char)" do
      before { message.content = "a" * 121 }
      it { should_not be_valid }
    end
  end
   
end
