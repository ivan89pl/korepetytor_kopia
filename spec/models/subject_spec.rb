# == Schema Information
#
# Table name: subjects
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  level                :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  tutor_subjects_count :integer          default(0)
#

require 'spec_helper'

describe Subject do
  let(:tutor) { prepare_tutor } 
  let(:subject) { tutor.subjects.create(FactoryGirl.attributes_for(:subject))}
  
  subject { subject }
  it { should respond_to(:name) }
  it { should respond_to(:level) }
  it { should be_valid }
  
  

  
  describe "methods" do
    describe ".full_name" do
      let(:joined_string) { subject.name + " - " + subject.level }
      it { subject.full_name.should match( joined_string ) }                                  
    end
  end
  
end
