# == Schema Information
#
# Table name: tutors
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  surname             :string(255)
#  degree              :string(255)
#  about               :text
#  online              :boolean          default(FALSE)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  price               :integer          default(1)
#  comments_count      :integer          default(0)
#  rating              :float
#  sort_order          :integer          default(0)
#  short_description   :text
#

require 'spec_helper'

describe Tutor do
  let(:user) { FactoryGirl.create(:user) }
  let(:tutor) { user.create_tutor(FactoryGirl.attributes_for(:tutor)) } 
  
  
  subject { tutor }
  it { should respond_to(:name) }
  it { should respond_to(:surname) }
  it { should respond_to(:degree) }
  it { should respond_to(:about) }
  it { should be_valid }
  

 
  describe "not accessible attributes" do
    it "not allow access to user_id" do 
      expect { Tutor.new(user_id: "1") }.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end
  
  describe "validations" do
    context "nil name" do
      before { tutor.name = nil }
      it { should_not be_valid }
    end
    context "empty name" do
      before { tutor.name = " " }
      it { should_not be_valid }
    end
    context "too long name" do
      before { tutor.name = "a" * 21  }
      it { should_not be_valid }
    end
    context "too long surname" do
      before { tutor.surname = "a" * 31 }
      it { should_not be_valid }
    end  
  end 
  
end
