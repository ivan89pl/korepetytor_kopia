
def sign_in(user)
  visit new_user_session_path
  fill_in "user_email", with: user.email
  fill_in "user_password", with: "secret"  
  click_button "Zaloguj"      
end

def prepare_tutor 
  user =  FactoryGirl.create(:user)
  user.create_room(FactoryGirl.attributes_for(:room)) 
  user.create_tutor(FactoryGirl.attributes_for(:tutor)) 
end

def add_subject(tutor)
  tutor.subjects.create(FactoryGirl.attributes_for(:subject))
end

def prepare_private_room(tutor, learner)
  tutor.create_private_room( sessionId: "14685d1ac5907f4a2814fed28294d3f797f34955", learner_id: learner.id)
end