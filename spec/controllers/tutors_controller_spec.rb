require 'spec_helper'

describe TutorsController do

  describe "authenticate" do
    let(:tutor_one) { prepare_tutor }
    let(:tutor_two) { prepare_tutor }
    
    subject { response }
    
    describe "for guest" do
      context "when hit show" do
        before { get :show, user_id: tutor_one.user }
        it { should render_template("show") }
      end
      context "when hit edit" do
        before { get :edit, user_id: tutor_one.user }
        it { should redirect_to(new_user_session_path) }
      end  
      context "when hit new" do
        before { get :new , user_id: tutor_one.user}
        it { should redirect_to(new_user_session_path) }
      end    
    end
    
    describe "for owner" do
      before { sign_in tutor_one.user }
      context "when hit show" do
        before { get :show, user_id: tutor_one.user }
        it { should render_template("show") }
      end
      context "when hit edit" do
        before { get :edit, user_id: tutor_one.user }
        it { should render_template('edit') }        
      end
      context "when hit new" do
        before { get :new, user_id: tutor_one.user }
        it { should render_template('new') }        
      end      
    end
    
    describe "for other tutor" do
      before { sign_in tutor_one.user }
      context "when hit show" do
        before { get :show, user_id: tutor_two.user }
        it { should render_template("show") }
      end
      context "when hit edit" do
        before { get :edit, user_id: tutor_two.user }
        it { should redirect_to(root_path) }
      end
      context "when hit new" do
        before { get :new, user_id: tutor_two.user }
        it { should redirect_to(root_path) }
      end
    end
  end
end
