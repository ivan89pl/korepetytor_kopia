FactoryGirl.define do

  factory(:user) do |f| 
    f.sequence(:email) { |n| "foo#{n}@example.com" }
    f.sequence(:nickname) { |n| "guest#{n}" }
    f.password "secret"
  end

  factory(:room) do |f| 
    f.name "testRoom"
    f.sessionId "14685d1ac5907f4a2814fed28294d3f797f34955"
  end

  factory(:message) do |f| 
    f.content "test message"

  end
  
  factory(:tutor) do |f| 
    f.name "Jan"
    f.surname "Kowalski"
    f.degree "Podstawowe"
    f.about "Jestem najmadrzejszy na swiecie"
    f.short_description "ble ble ble ble ble"
  end
  
  factory(:subject) do |f| 
    f.sequence(:name) { |n| "English#{n}" }
    f.sequence(:level) { |n| "good#{n}" }   
  end
  
  factory(:private_room) do |f| 
    f.sessionId "14685d1ac5907f4a2814fed28294d3f797f34955" 
  end
  
end