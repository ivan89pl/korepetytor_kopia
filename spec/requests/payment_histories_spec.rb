#!/bin/env ruby
# encoding: utf-8
require 'spec_helper'

describe "PaymentHistories" do

  after(:all)      { User.delete_all }
  
  let(:learner)           { FactoryGirl.create(:user, :token => 10) }
  let(:tutor)             { prepare_tutor  }
  #let(:private_room)      { tutor.create_private_room( sessionId: "14685d1ac5907f4a2814fed28294d3f797f34955", learner_name: learner.nickname )} #prepare_private_room(tutor, learner)  }
  

  before do
    sign_in tutor.user     
    visit new_tutor_private_room_path(tutor)
    fill_in "private_room_learner_name", with: learner.nickname 

    click_button "Zatwierdź"
    click_link "Wyloguj"
      
  end 
    

  
  
  describe "#show page" do
    describe "for learner" do
      before do
        sign_in learner      
        visit tutor_private_room_path(tutor)
        #save_and_open_page
      end 
    
      subject { page }
      it { should have_selector('title', text: "Prywatna lekcja") } 
      it { should have_selector('h4', text: "Korepetytor: #{tutor.user.nickname}") }
      it { should have_selector('h4', text: "Uczeń: #{learner.nickname}") }
      it { should have_selector('#privTutor #videobox') }
      it { should have_selector('#privLearner #videobox2') } 
      it { should have_selector('#privChat .input-append') } 
      it { should have_selector('a', text: "Dodaj minuty") } 
      
      describe "click on 'Dodaj minuty'" do
        
#        it "should increment the other user's followers count" do
#          expect do
#            click_link "Dodaj minuty"
#          end.to change(learner, :token).by(-1)
#        end
        
      end
    end
  end
end
