#!/bin/env ruby
# encoding: utf-8
require 'spec_helper'
 
describe "Tutor Pages" do
  
  after(:all)     { User.delete_all }
  
  let(:user)      { FactoryGirl.create(:user) }
  let(:tutor_atr) { FactoryGirl.attributes_for(:tutor) }
  
  before do
    sign_in user
  end   
    
  describe "#new page" do
    before do
      click_link "zostan korepetytorem" #TODO: zmienic na path jak bedzie dostepny
    end  
    subject { page }
       
    it { should have_selector('title', text: "Zostań korepetytorem") }    
    it { should have_selector('legend', text: "Zostań korepetytorem") }
  
    describe "tutor creation" do
      before do    
        fill_in "tutor_name", with: tutor_atr[:name]
        fill_in "tutor_surname", with: tutor_atr[:surname]
        select('Podstawowe', :from => 'tutor_degree')
        fill_in "tutor_about", with: tutor_atr[:about] 
        fill_in "tutor_short_description", with: tutor_atr[:short_description] 
        #save_and_open_page
      end
      it "create tutor" do
        expect { click_button "Zatwierdź" }.to change(Tutor, :count).by(1)
      end

      context "redirect after create" do
        before { click_button "Zatwierdź" }
        it {current_path.should eq(new_user_room_path(user)) }
      end
    end
  end
 
  
  describe "#show page" do
    
    let(:tutor) { user.create_tutor(FactoryGirl.attributes_for(:tutor)) }

    before do
      user.create_room(FactoryGirl.attributes_for(:room))

      add_subject(tutor)
      add_subject(tutor)
      visit user_tutor_path(user)   
    end  
    
    subject { page }
    it { should have_selector('title', text: "Mój Profil") }         
    it { should have_selector('h2', text: user.nickname) }
    it { should have_content(tutor.degree) }
    it { should have_selector('h3', text: "O mnie:") }
    it { should have_selector('.profileInformation dt', text: "Wykształcenie") }
    it { should have_selector('.profileInformation dd', text: tutor.degree) }
    it { should have_content(tutor.subjects.first.name )    }
    describe "list of tutor subjects" do
      it { should have_selector('.profileInformation dt', text: "Udzielam korepetycje z") }
      it { should have_selector('.profileInformation dd', text: tutor.subjects.first.name) }
      it { should have_selector('.profileInformation dd', text: tutor.subjects.last.name) }
    end
  
    describe "for guest" do
      before do
        click_link "Wyloguj"
        visit user_tutor_path(user) 
      end
      subject { page }
      it { should_not have_link("Edytuj te dane", href: edit_user_tutor_path(user)) }
      it { should_not have_link("Edytuj dane konta", href: edit_user_registration_path) }
    end
    
    describe "for owner" do         
      before do    
        click_link "Mój Profil"
        #save_and_open_page
      end     
      subject { page }
      it { should have_link("Edytuj te dane", href: edit_user_tutor_path(user)) }
      it { should have_link("Edytuj dane konta", href: edit_user_registration_path) }
    end    
  end
    

  describe "#edit page" do
    
    before do
      user.create_tutor(FactoryGirl.attributes_for(:tutor))
      user.create_room(FactoryGirl.attributes_for(:room))
      visit user_tutor_path(user) 
      click_link "Edytuj te dane"      
    end
    
    subject { page }
    it { should have_selector('title', text: "Edycja wizytówki") }         
    it { should have_selector('legend', text: "Edytuj wizytówkę") }
    
    describe "editing tutor" do
      let(:new_name_for_tutor) {"Johny"}
      let(:new_surname_for_tutor) {"Beep"}
      let(:new_about_for_tutor) {"I'm from USA"}
      let(:new_degree_for_tutor) {"Wyższe"}
      
      before do
        fill_in "tutor_name", with: new_name_for_tutor 
        fill_in "tutor_surname", with: new_surname_for_tutor
        select( new_degree_for_tutor , :from => 'tutor_degree')
        fill_in "tutor_about", with: new_about_for_tutor
        click_button "Zatwierdź"
        #save_and_open_page
      end
      subject { page }
#      it{ should have_selector('h2', text: new_name_for_tutor)}
#      it{ should have_selector('h2', text: new_surname_for_tutor)}
      it{ should have_content( new_degree_for_tutor )}
      it{ should have_content( new_about_for_tutor )}
    end
    #TODO: tests for Subjects (java)
    
  end



  
end
  
  

