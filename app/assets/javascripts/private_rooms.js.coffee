# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('#connectLink').click ->
    connect() 
  $('#publishLink').click ->
    publish() 
  $('#unpublishLink').click ->
    unpublish()
  $('strong#annouce').click ->
    $('strong#annouce').hide()

  $ ->
    unless ( document.getElementById('timer') == null  )
      minutes = $('#timer').text().match(/[0-9]+/)
      start_counting(minutes) unless ( minutes == null )

      