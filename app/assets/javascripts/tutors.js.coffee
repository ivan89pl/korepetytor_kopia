# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $replace_id = (txt) ->
    matches = txt.match(/[0-9]+/)    
    inc_val = parseInt(matches)     
    txt.replace(/\d+/, ++inc_val) 

  $('form').on 'click', '.remove_fields', (event) ->
    $(this).prev('input[type=hidden]').val('1')
    $(this).closest('.subject').hide()
    event.preventDefault()
  $('form').on 'click', '.add_fields', (event) ->
    select_id = $('#subject_children li:last select').attr("id")
    select_name = $('#subject_children li:last select').attr("name")
    input_id = $('#subject_children li:last input').attr("id")
    input_name = $('#subject_children li:last input').attr("name")

    $('#subject_children li:last').clone()
    .show()
    .find('select')
    .attr('id', $replace_id(select_id))
    .attr('name', $replace_id(select_name))
    .end().find('option')
    .removeAttr('selected')
    .end().find('input')
    .val('false')
    .attr('id', $replace_id(input_id))
    .attr('name', $replace_id(input_name))
    .end().appendTo('#subject_children');
    event.preventDefault()
