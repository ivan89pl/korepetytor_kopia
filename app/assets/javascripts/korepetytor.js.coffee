jQuery ->
  $("#headerPageTitle").click ->
     window.location=$(this).find("a").attr("href")
     return false

  if $('form textarea.wysiwyg').length
    $('form textarea.wysiwyg').wysiwyg {

      rmUnusedControls: true,
      initialContent: "",
      plugins: {
        autoload: true
        }
      controls: {
        bold: { visible : true },
        italic: { visible : true },
        underline: { visible : true },

        insertUnorderedList: { visible : true },
        insertOrderedList: { visible : true },
        

        html: { visible : true },
        
        justifyLeft: { visible : true },
        justifyCenter: { visible : true },
        justifyRight: { visible : true }
        }
      }     