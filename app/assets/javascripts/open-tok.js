var VIDEO_WIDTH = 398;
var VIDEO_HEIGHT = 297;


var session;
var publisher;
var subscribers = {};


//--CHECKING-SYS-REQ-AND-EXCEPTION-HANDLER--//
TB.setLogLevel(TB.DEBUG);
TB.addEventListener("exception", exceptionHandler);

if (TB.checkSystemRequirements() != TB.HAS_REQUIREMENTS) {
  alert('Zainstaluj najnowszą wersje flasha!');
}

function controlSession(){
  session = TB.initSession(sessionId);

  
  session.addEventListener('sessionConnected', sessionConnectedHandler);
  session.addEventListener('sessionDisconnected', sessionDisconnectedHandler);
  session.addEventListener('connectionCreated', connectionCreatedHandler);
  session.addEventListener('connectionDestroyed', connectionDestroyedHandler);
  session.addEventListener('streamCreated', streamCreatedHandler);
  session.addEventListener('streamDestroyed', streamDestroyedHandler);
  
 


}

//--BUTTON-FUNCTIONS--//
function connect() {
  $("span#connectionStatus").text("Łączenie.. proszę czekać.");
  controlSession()
  session.connect(apiKey, token);

}

function disconnect() {
  session.disconnect();
}

function publish() {
  
  if(!publisher) {
    var div = document.createElement('div');
    div.setAttribute('id', 'publisher');    
    var publisherContainer = choiceDiv();        
    publisherContainer.appendChild(div);
  } 

  var publisherProps = {
    width: VIDEO_WIDTH, 
    height: VIDEO_HEIGHT,
    publishVideo: choiceVideo(),
    publishAudio: choiceAudio()
    
  };
  publisher = TB.initPublisher(apiKey, 'publisher', publisherProps);
  publisher.addEventListener('accessDenied', accessDeniedHandler);
  publisher.addEventListener('accessAllowed', accessAllowedHandler);
   
}

function unpublish(){
  if (publisher) {
    session.unpublish(publisher);
  }
  publisher = null;
  show('publishLink');
  show('radioGroup')
  hide('unpublishLink');
  
}


//--OPENTOK-EVENT-HANDLER-FUNCTIONS--//



function accessDeniedHandler(event) {

  $("span#connectionStatus").text("Zablokowany dostęp do kamery!");
  $("#videobox").remove();
  //TODO: tutaj jeszcze trzeba popracowac
  publisher = null;
  
}

function accessAllowedHandler(event) {
  session.publish(publisher);
  hide('publishLink');
  hide('radioGroup');
}



function sessionConnectedHandler(event) {
  // Subscribe to all streams currently in the Session
  for (var i = 0; i < event.streams.length; i++) {
    addStream(event.streams[i]);
  }
  
  //zmienia status korepetytora na ONLINE
    if (session.connection.data == "publisher" && tutor != false) {
    setOnlineStatusHandler();
    setOnlineStatus();
  }
  
  
  
  
  if ((event.streams.length == 0) && (viewForLearner == true)) {
    $("span#connectionStatus").text("Nie wykryto żadnych przekazów wideo.");
  } else if (event.streams.length == 0){
    $("span#connectionStatus").text("Korepetytor aktualnie ma wyłączony przekaz wideo");

  }else {
    $("span#connectionStatus").text("Działa. Nawiązano połączenie.");
  }
  //show('disconnectLink');
  show('publishLink');
  show('radioGroup')
//hide('connectLink');
 
}
function streamCreatedHandler(event) {
  // Subscribe to the newly created streams
  for (var i = 0; i < event.streams.length; i++) {
    addStream(event.streams[i]);
  }	
  $("span#connectionStatus").text("Działa. Nawiązano połączenie.");

}

function streamDestroyedHandler(event) {

  if($('#roomTitle').length){
    $("span#connectionStatus").text("Korepetytor wyłączył przekaz wideo."); 
  }

  
 
//  $('#videobox').css('height', '0px');
  

// This signals that a stream was destroyed. Any Subscribers will automatically be removed.
// This default behaviour can be prevented using event.preventDefault()
}

function sessionDisconnectedHandler(event) {
  // This signals that the user was disconnected from the Session. Any subscribers and publishers
  // will automatically be removed. This default behaviour can be prevented using event.preventDefault()
  $("span#connectionStatus").text("Rozłączono. Spróbuj połączyć się ponownie.");
  publisher = null;
  show('connectLink');
  //hide('disconnectLink');
  hide('publishLink');
  hide('unpublishLink');

}

function connectionDestroyedHandler(event) {
// This signals that connections were destroyed
}

function connectionCreatedHandler(event) {
// This signals new connections have been created.

}

function exceptionHandler(event) {
  if (event.code === 1008 ) {
    $("span#connectionStatus").text("Brak odpowiedzi z serwera.. Ponowna próba za parę sekund.");
  } else {
    $("span#connectionStatus").text(event.message);		
  }
}




//--HELPERS--//

function addStream(stream) {
  if (stream.connection.connectionId == session.connection.connectionId) {
    show('unpublishLink');
    return;
  }


  var publisherContainer;
  if (stream.connection.data == "learner") {
    publisherContainer = document.getElementById('videobox2'); 
  } else  {
    publisherContainer = document.getElementById('videobox'); 
  }
    
   
  var div = document.createElement('div');
  var divId = stream.streamId;
  div.setAttribute('id', divId);

  publisherContainer.appendChild(div);   
  var subscriberProps = {
    width: VIDEO_WIDTH, 
    height: VIDEO_HEIGHT
  };
    
  subscribers[stream.streamId] = session.subscribe(stream, divId, subscriberProps);
}

function show(id) {
  var loc = document.getElementById(id);
  if( loc == null) {
    return 
  } else {
    loc.style.display = 'block';
  }
}

function hide(id) {
  var loc = document.getElementById(id);
  if(loc == null) {
    return 
  } else {
    loc.style.display = 'none';
  }
}

function choiceVideo() {
  if (document.getElementById("audOnly").checked) {
    return false;
  }
  return true;
}

function choiceAudio() {
  if (document.getElementById("vidOnly").checked) {
    return false;
  }
  return true;
}

function choiceDiv() {
  var pubContainer;
  if(viewForLearner == true){
    pubContainer = document.getElementById('videobox2');
  }else {
    pubContainer = document.getElementById('videobox');
  }
  return pubContainer;

}