# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('#connectLink').click ->
    connect() 
  $('#publishLink').click ->
    publish() 
  $('#unpublishLink').click ->
    unpublish()

  $('input#audOnly').click ->
    $("label.radio").children().removeAttr("checked")
    $(this).attr("checked", "true")
  $('input#vidOnly').click ->
    $("label.radio").children().removeAttr("checked")
    $(this).attr("checked", "true")
  $('input#audVid').click ->
    $("label.radio").children().removeAttr("checked")
    $(this).attr("checked", "true")    