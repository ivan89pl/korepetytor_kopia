# encoding: utf-8
# #!/bin/env ruby
# == Schema Information
#
# Table name: private_rooms
#
#  id           :integer          not null, primary key
#  sessionId    :string(255)
#  elapsed_time :time
#  learner_id   :integer
#  tutor_id     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  expire_in    :datetime
#

class PrivateRoom < ActiveRecord::Base
  begin :attributes
    attr_accessible :elapsed_time, :sessionId, :learner_name
    attr_accessor :learner_name
  end
  begin :relations
    belongs_to :tutor 
    has_many :messages, :as => :conversation, :dependent => :destroy
  end
  
  begin :validations
    validate :learner_name , :learner_name_valid, :unless => "errors.any?"
  end
  
  begin :filters
    before_create :set_learner_id
  end
  
  scope :active_for_learner, lambda { |user_id| where("learner_id = ? AND updated_at > ?", user_id, Time.now - CONST[:priv_room_expire_in].minutes).limit(5) }

  public #TODO: PUBLIC??
  
  def add_time!
    if self.expire_in == nil or self.expire_in < Time.now #expired
      self.expire_in = Time.now + 15.minutes
    else
      self.expire_in += 15.minutes
    end
    self.save!(:validate => false)
  end
  
  def minutes_left
    if self.expire_in == nil or self.expire_in < Time.now
      nil
    else
      time = self.expire_in - Time.now
      time = time / 1.minutes
      time.round
    end
  end

  def expired?
    true if self.updated_at < Time.now - CONST[:priv_room_expire_in].minutes
  end
  
  private
  
  def learner_name_valid
    user = User.find_by_nickname(learner_name)
    if user.nil?
      errors.add(:learner_name_valid,'Nie znaleziono użytkownika o podanym nicku')
    elsif user.tutor?
      errors.add(:learner_name_valid,'Wskazany użykownik jest korepetytorem. Nie można go zaprosić')
    end
  end
  
  def set_learner_id
    return true if self.learner_name.nil?
    name = self.learner_name 
    learner = User.find_by_nickname(name)
    self.learner_id = learner.id
  end
end
