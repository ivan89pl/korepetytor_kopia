# == Schema Information
#
# Table name: payment_notifications
#
#  id             :integer          not null, primary key
#  params         :text
#  status         :string(255)
#  transaction_id :string(255)
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class PaymentNotification < ActiveRecord::Base

  belongs_to :user
  serialize :params
  after_create :add_tokens

  def add_tokens
    if status == "Completed" && params[:secret] == CONFIG[:paypal_secret]
      user = User.find_by_id(params[:invoice])
      user.add_token!(params[:tokens].to_i)
    end
  end
end
