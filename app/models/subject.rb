# == Schema Information
#
# Table name: subjects
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  level                :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  tutor_subjects_count :integer          default(0)
#

class Subject < ActiveRecord::Base
  
  begin :attributes
    attr_accessible :level, :name
  end
   
  begin :relations
    has_many :tutor_subjects
    has_many :tutors, :through => :tutor_subjects
  end

  begin :scopes
    scope :all_by_popularity, lambda { where("id != 1").order("tutor_subjects_count DESC") }
 
  end
  
  def full_name
    self.name + " - " + self.level
  end
  
end
