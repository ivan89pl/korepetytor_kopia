# == Schema Information
#
# Table name: payment_histories
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  sender_id  :integer
#  token      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PaymentHistory < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :user
end
