# == Schema Information
#
# Table name: comments
#
#  id              :integer          not null, primary key
#  sender_id       :integer
#  tutor_id        :integer
#  private_room_id :integer
#  rate            :integer
#  content         :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Comment < ActiveRecord::Base
  
  begin :attributes
    attr_accessible :content, :rate
  end
  
  begin :relations 
    belongs_to :tutor
    belongs_to :user, :foreign_key => "sender_id"
  end
  
  scope :unpublished, lambda { |user_id| where("sender_id = ? AND rate IS NULL", user_id) }
  scope :published, where("rate IS NOT NULL") 
  
  begin :validations
    validates :content,             :presence => true ,  :allow_blank => false,  :allow_nil => false, 
      :length => {:maximum => 150,  :allow_blank => false, :allow_nil => false} 
                                  
    validates :rate,                :presence => true  ,  :allow_blank => false,  :allow_nil => false   
  end
  
  public
  
  def self.prepare_empty_comment!(tutor, learner, private_room)
    unless self.comment_for_lesson_exist?(private_room)
      c =  self.new
      c.tutor_id = tutor
      c.sender_id = learner
      c.private_room_id = private_room
      c.save!(:validate => false)
    end
    
  end

  
  private
  
  def self.comment_for_lesson_exist?(private_room)
    true if self.find_by_private_room_id(private_room)   
  end
  
  
end
