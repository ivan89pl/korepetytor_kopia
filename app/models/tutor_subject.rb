# == Schema Information
#
# Table name: tutor_subjects
#
#  id         :integer          not null, primary key
#  subject_id :integer
#  tutor_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TutorSubject < ActiveRecord::Base
  begin :attributes
    attr_accessible :subject_id
  end
   
  begin :relations
    belongs_to :tutor
    belongs_to :subject, :counter_cache => true
  end
  
   validates :subject_id,  :uniqueness => {:scope => :tutor_id, :message => "Wybrano jeden przedmiot wiecej niz raz"}
end
