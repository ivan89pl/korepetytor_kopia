# == Schema Information
#
# Table name: rooms
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  sessionId  :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Room < ActiveRecord::Base
  
  begin :attributes
  attr_accessible :name, :sessionId
  end
  
  begin :validations
  validates :sessionId, :presence => true,  :uniqueness => true  
  validates :name,      :presence => true,  :uniqueness => true  
  end
  
  begin :relations
  belongs_to :user
  has_many :messages, :as => :conversation, :dependent => :destroy
  end

end
