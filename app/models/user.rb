# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  nickname               :string(255)
#  token                  :integer          default(0)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  begin :attributes
    attr_accessible :email, :password, :password_confirmation, :remember_me
    attr_accessible :nickname
  end
  
  begin :relations
    has_one :room, :dependent => :destroy
    has_one :tutor, :dependent => :destroy
    has_many :payment_histories
  
  end 
  
  begin :validations
    validates :nickname, 
      :presence => true ,  :allow_blank => false,  :allow_nil => false, :uniqueness => true,
      :length => {:maximum => 20, :minimum => 6,  :allow_blank => false, :allow_nil => false}   
  end
  
  public
  #sprawdza czy uzytkownik jest korepetytorem
  def tutor?
    true if self.tutor.present? and self.room.present?
  end
  #sprawdza czy uzytkownik ma wystarczajaca liczbe orderow zeby zaplacic za sesje
  def enough_tokens?(amount)
    true if self.token >= amount
  end
  
  #sprawdza czy w ogole ma jakies ordery
  def has_tokens?
    true if self.token > 0
  end

  #sprawdza czy istnieją kometarze do wystawienia
  def has_unpublished_comments?
    true unless Comment.unpublished(self.id).limit(1).empty?  
  end  

  #TODO: PUBLIC???
  #--start--TE FUNKCJE WYWOLYWANE TYLKO W PaymentHistoriesController # create
  #dodaje uzytkownikowi ordery
  def add_token!(value)
    self.token = self.token.to_i + value
    self.save!
  end
  #odejmuje uzytkownikowi ordery
  def take_token!(value)
    self.token = self.token.to_i - value
    self.save!
  end
  #--koniec--
  
  #konwertuje liczbe orderow na minuty ( 1 order = 15min)
  def token_to_minutes
    self.token * 15
  end
  
  #--start-- obsluga PayPal
  
  PAYPAL_CERT_PEM = File.read("#{Rails.root}/certs/paypal_cert.pem")
  APP_CERT_PEM = File.read("#{Rails.root}/certs/app_cert.pem")
  APP_KEY_PEM = File.read("#{Rails.root}/certs/app_key.pem")

  def encrypt_for_paypal(values)
    signed = OpenSSL::PKCS7::sign(OpenSSL::X509::Certificate.new(APP_CERT_PEM),        OpenSSL::PKey::RSA.new(APP_KEY_PEM, ''), values.map { |k, v| "#{k}=#{v}" }.join("\n"), [], OpenSSL::PKCS7::BINARY)
    OpenSSL::PKCS7::encrypt([OpenSSL::X509::Certificate.new(PAYPAL_CERT_PEM)], signed.to_der, OpenSSL::Cipher::Cipher::new("DES3"),        OpenSSL::PKCS7::BINARY).to_s.gsub("\n", "")
  end
  
  def paypal_encrypted(item_name, price, user,  return_url, notify_url)
    values = {
      :business => CONFIG[:paypal_email],
      :cmd => '_xclick',
      :upload => 1,
      :return => return_url,
      :cancel_return => return_url,
      :invoice => user.id,
      :cpp_logo_image => CONFIG[:paypal_logo],
      :currency_code => "PLN",
      :lc => "PL",
      :notify_url => notify_url,
      :cert_id => CONFIG[:paypal_cert_id],
        
        
      :item_name => item_name,   
      :amount => price,
    }
 
  
    encrypt_for_paypal(values)
  end
  
  #--koniec-- obsluga PayPal
  
  
  
end
