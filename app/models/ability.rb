class Ability
  include CanCan::Ability

  def initialize(user)


   user ||= User.new # guest user
   
    

     can :manage, Tutor,  :user_id => user.id #if user is owner
     can :manage, Room,  :user_id => user.id #if user is owner
     can :manage, User,  :id => user.id #if user is owner

  end
end
