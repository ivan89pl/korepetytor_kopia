# encoding: utf-8
# == Schema Information
#
# Table name: tutors
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  surname             :string(255)
#  degree              :string(255)
#  about               :text
#  online              :boolean          default(FALSE)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  price               :integer          default(1)
#  comments_count      :integer          default(0)
#  rating              :float
#  sort_order          :integer          default(0)
#  short_description   :text
#  last_seen_at        :datetime
#

# #!/bin/env ruby

class Tutor < ActiveRecord::Base
  
  
  begin :attributes
    attr_accessible :about, :name, :online, :surname, :degree, :avatar, :short_description, :price, :comments_count, :rating, :last_seen_at , :tutor_subjects_attributes
    
  end
  
  begin :acts
    has_attached_file :avatar, :styles => { :big => "150x150>", :medium => "75x75>", :small => "37x37>" },
      :default_url => ActionController::Base.helpers.asset_path('missing_:style.jpg')
  end
  
  begin :relations
    has_one :private_room,:dependent => :destroy
    belongs_to :user 
    has_many :tutor_subjects
    has_many :subjects, :through => :tutor_subjects
    has_many :comments, :dependent => :destroy
  end 
  
  define_index do
    indexes :short_description
    indexes user.nickname, as: :user_nickname
  end


  scope :only_online, lambda { where("last_seen_at > ?", Time.now ) }  
  scope :only_offline, lambda { where("last_seen_at < ?", Time.now ) }  
  default_scope lambda { order("sort_order DESC").limit(20) }
  #merged_scope = scope1.merge(scope2)
  
 
  accepts_nested_attributes_for :tutor_subjects, :allow_destroy => true,  :reject_if => proc { |attributes| attributes['subject_id'].to_i == 1  }

  begin :validations
    validates_attachment :avatar,      :content_type => { :content_type => /image/ , :message => "Zły format. Plik musi być zdjęciem"},
                                       :size => { :in => 0..50.kilobytes , :message => "Za duży plik! max 50KB "}
                                     
    #validate :avatar,                  :avatar_dimensions, :unless => "errors.any?"
    
    validates :name,                   :presence => true ,  :allow_blank => false,  :allow_nil => false, 
                                       :length => {:maximum => 20,  :allow_blank => false, :allow_nil => false} 
    validates :surname,                :length => {:maximum => 30}   
    
#    validates :short_description,      :presence => true ,  :allow_blank => false,  :allow_nil => false, 
#                                       :length => {:maximum => 150,  :allow_blank => false, :allow_nil => false}  
    
  end
  

  
  public
  
  #sprawdza czy korepetytor jest online
  def online?
    self.last_seen_at > Time.now
  end
  
  #ustawia status online na 35sek,
  #funkcja wywolywana w tutors#online przez $.ajax co 30sekund
  #initializacja w open-tok.js fun SessionConnectedHandler tylko dla korepetytora
  #interval w intervals_for_room.js
  def set_online_status!
    self.last_seen_at =  35.seconds.from_now
    self.save!
    #self.toggle!(:online) unless self.online?
  end
  
  #akcje wykonywane po dodaniu oceny i komentarza
  def new_comment_handler!(rate)
    #ponowne wyliczenie OCENY_OGÓLNEJ
    self.rating = new_rating(self, rate)
    #zwiększenie licznika komentarzy
    self.comments_count += 1
    #sort order wyliczany na podstawie OCENA_OGÓLNA * LICZBA_KOMENTARZY
    self.sort_order = self.rating * self.comments_count
    self.save!
  end
  
  #sprawdza czy korepetytor ma jakies wystawione komentarze
  def has_comments?
    true unless self.comments_count == 0
  end
  
  
  private
  
  #oceny wyliczane ze zwykłej średniej arytmetycznej
  def new_rating(obj, add_rate)
    return add_rate if obj.rating.nil? #gdy pierwsza ocena
    sum =  ( obj.comments_count * obj.rating  + add_rate) / ( obj.comments_count + 1 )
    sum.round(1)
  end
  
  
  def avatar_dimensions
    #odrzuca avatary mniejsze niz 150x150px
    return nil unless self.avatar.present?
    dimensions = Paperclip::Geometry.from_file(avatar.queued_for_write[:original].path)   
    if dimensions.width < 149 and dimensions.height < 149
      errors.add(:avatar_dimensions,'Minimalna wysokość i szerokość zdjęcia to 150x150 px')
    end 
  end
 

  
end
