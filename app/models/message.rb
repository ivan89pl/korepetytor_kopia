# == Schema Information
#
# Table name: messages
#
#  id                :integer          not null, primary key
#  content           :text
#  sender            :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  conversation_id   :integer
#  conversation_type :string(255)
#

class Message < ActiveRecord::Base
  
  begin :attributes
    attr_accessible :content
  end
  
  begin :validations
   validates :content, 
      :presence => true , :allow_blank => false,  :allow_nil => false,
      :length => {:maximum => 120,  :allow_blank => false, :allow_nil => false} 
  end
  
  begin :relations
    
   belongs_to :conversation, :polymorphic => true
  end

end
