module ApplicationHelper
    
  def errors_for(form, field)
    unless form.object.errors[field].nil?
      err = content_tag(:ul, :class => "fieldErrors") do
        str = ""
        form.object.errors[field].each do | e |
          str << content_tag(:li) do             
            "#{e}."
          end
        end
        str.html_safe
      end
      return err.html_safe
    end
  end
  
  def twitterized_type(type)
    case type
    when :alert
      "alert-error"
    when :error
      "alert-error"
    when :notice
      "alert-info"
    when :success
      "alert-success"
    when :messeges
      "alert-error"
    else
      type.to_s
    end
  end
  
  #sprawdza czy uzytkownik nie przerwal tworzenia profilu korepetytora w polowie.
  def register_tutor_helper
    current_user.tutor.present? ? new_user_room_path(current_user.id) : new_user_tutor_path(current_user)
  end

end
