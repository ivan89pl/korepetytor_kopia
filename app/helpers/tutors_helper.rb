module TutorsHelper
  def star_rating(rating)
    if rating.nil?
      #"starRating_0"
      "Star0.jpg"
    else
    #"starRating_" + "#{rating.round}"
    "Star" + "#{rating.round}" + ".jpg"
  end
  end
end
