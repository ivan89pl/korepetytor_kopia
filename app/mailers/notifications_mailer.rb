class NotificationsMailer < ActionMailer::Base

  default :from => "ivan89pl@gmail.com"
  default :to => "ivan89pl@gmail.com"

  def new_message(message)
    @message = message
    mail(:subject => "[Korepetytor] - #{message.name} - #{message.subject}")
  end

end
#:name, :email, :subject, :body, :tokens_amount