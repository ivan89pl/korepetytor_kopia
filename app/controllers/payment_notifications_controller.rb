class PaymentNotificationsController < ApplicationController
  
  protect_from_forgery :except => [:create]
  before_filter :hack_shield
  
  def create   
    create_handler(params)
    render :nothing => true
  end

  private
  
  #sprawdza czy ktos nie bawil sie z linkami
  def hack_shield
    if params[:secret] != CONFIG[:paypal_secret]
      params[:payment_status] = "HACKED"
    end         
  end
  
  #tworzy nowy rekord w bazie
  def create_handler(params)
    pn =  PaymentNotification.new
    pn.params = params
    pn.user_id = params[:invoice]
    pn.status = params[:payment_status]
    pn.transaction_id = params[:txn_id]
    pn.save!
  end
  
end
