class MyProfilesController < ApplicationController
  before_filter :access_control

  def show
    @user = find_user(params[:user_id])
    @private_rooms = find_rooms(params[:user_id])
    
  end  
  
  private
  
  def find_user(user_id)
    User.find_by_id(user_id)
  end

  def find_rooms(user_id)
  	priv = PrivateRoom.active_for_learner(user_id)
  	return destroy_expired_room(priv)
  end

end
