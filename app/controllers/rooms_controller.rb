# encoding: utf-8
# #!/bin/env ruby
class RoomsController < ApplicationController
  before_filter :signed_in, :except => [ :show, :index ]
 
  def new
    user = find_user(params[:user_id])
    @room = user.build_room
    authorize!(:manage, @room)
  end
  
  def create
    user = find_user(params[:user_id])
    opentok = config_opentok
    session = opentok.create_session request.remote_addr
    params[:room][:sessionId] = session.session_id
    @room = user.build_room(params[:room])
    authorize!(:manage, @room)

    respond_to do |format|
      if @room.save
        format.html { redirect_to user_tutor_path(user) }
      else
        format.html { 
          flash[:error] = "Nieznany błąd, spróbuj ponownie."
          redirect_to new_user_room_path(user) } 
      end
    end
  end
  
  def show
    user = find_user(params[:user_id])
    @room = user.room
    @tutor = user.tutor
    opentok = config_opentok   
    @ot_token = opentok.generate_token( session_id: @room.sessionId, role: find_role(user), connection_data: find_role(user) )
    
    @learner =find_user(@tutor.private_room.learner_id) if @tutor.private_room.present? 
  end
  
  def edit
    user = find_user(params[:user_id])
    @room = user.room
    authorize!(:manage, @room)
  end
  
  def update
    user = find_user(params[:user_id])
    @room = user.room
    authorize!(:manage, @room)
    respond_to do |format|
      if @room.update_attributes(params[:room])
        format.html { redirect_to user_room_path(user) }
      else
        format.html { render :action => "edit" } 
      end
    end
  end

  
  private
  def config_opentok       
    opentok = OpenTok::OpenTokSDK.new CONFIG[:api_key], CONFIG[:api_secret]
    opentok #TODO: usunac to
  end
  
  def find_user(user_id)
    User.find(user_id)
  end

  def find_role(user)
    if current_user == user
      ot_role = "publisher"
    else
      ot_role = "subscriber"   
    end
    ot_role
  end
  
  def signed_in
    unless user_signed_in?
      flash[:notice] = "Aby kontynuować musisz się zalogować."
      redirect_to new_user_session_path
    end
  end
  
end
