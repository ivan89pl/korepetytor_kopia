class PaymentHistoriesController < ApplicationController

  def create
    
    recipient = find_reciepient(params[:user_id])
    price = recipient.tutor.price
    
    if check_solvency(price) 
      @payment_history = recipient.payment_histories.new
      @private_room = recipient.tutor.private_room
      create_handler!(@payment_history, recipient,  @private_room)           
    end
    
    respond_to do |format|
      format.js { 
        if @payment_history.present?
          render :layout=>false
        else
          render :js => "alert('Nie masz wystarczajacej ilosci orderow zeby zaplacic za lekcje.');"           
        end}   
    end
  end
  
  private
  #szuka korepetytora, odbiorcy orderow
  def find_reciepient(user_id)
    User.find_by_id(user_id)
  end
  
  #sprawdza czy uczen ma z czego zaplacic za lekcje
  def check_solvency(value)
    true if current_user.enough_tokens?(value)
  end
  
  #obsluga akcji zaplaty za prywatna lekcje
  def create_handler!(payment_history, recipient, private_room)
    price = recipient.tutor.price
    
    #zapisuje dane transakcji do bazy    
    payment_history.sender_id = current_user.id
    payment_history.token = price
    payment_history.save 
    
    #oddaje okreslana ilosc orderow korepetytorowi z konta ucznia wykonujacego ta akcje
    recipient.add_token!(price)
    current_user.take_token!(price)
    
    #dodanie czasu do licznika prywatnej lekcji (1 klik = 15min)
    private_room.add_time!
     
    #przygotowuje komentarz ktory uczen po skonczonej lekcji moze wystawic, 
    #tylko jeden komentarz do jednej prywatnej lekcji
    #czyli nawet jak wykupi 10 razy minuty do jednej prywatnej lekcji, moze wystawic tylko jeden komentarz.
    Comment.prepare_empty_comment!(recipient.tutor.id, current_user.id, private_room.id)
  end    

end
