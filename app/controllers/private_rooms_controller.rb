# encoding: utf-8
# #!/bin/env ruby
class PrivateRoomsController < ApplicationController
  before_filter :signed_in
 
    
  def new
    tutor = find_tutor(params[:tutor_id])
    @private_room = tutor.build_private_room
  end
  
  def create
    tutor = find_tutor(params[:tutor_id]) 
    opentok = config_opentok
    #session_properties = {OpenTok::SessionPropertyConstants::P2P_PREFERENCE => "enabled"} 
    session = opentok.create_session(request.remote_addr)#, session_properties )
    params[:private_room][:sessionId] = session.session_id
    @private_room = tutor.build_private_room(params[:private_room] )
    
    respond_to do |format|
      if @private_room.save
        format.html { redirect_to tutor_private_room_path(tutor) }
      else
        format.html { render :action => "new" } 
      end
    end    
  end
  
  #powiadamia o utworzeniu prywatnej lekcji w pokoju ogolnym
  def annouce
    tutor = find_tutor(params[:tutor_id]) 
    protect_from_hack(tutor.user)
    @room = tutor.user.room
    @learner = find_user(tutor.private_room.learner_id)

    respond_to do |format|
      format.js
    end
  end
  
  def show
    @tutor = find_tutor(params[:tutor_id])
    @private_room = @tutor.private_room
    @learner = find_user(@private_room.learner_id)
    assign_access(@tutor.user, @learner)
    opentok = config_opentok  
    if current_user == @tutor.user #TODO: IMPORTANT usunac ta funkcje
      @var = "tutor"
    else
      @var = "learner"
    end
    @ot_token = opentok.generate_token(session_id: @private_room.sessionId, role: "publisher",  connection_data: @var.to_s  ) 
  end
   
  
  def destroy
    tutor = find_tutor(params[:tutor_id])
    tutor.private_room.destroy
    redirect_to user_room_path(tutor.user)
  end
  
  
  private
  
  def config_opentok       
    opentok = OpenTok::OpenTokSDK.new CONFIG[:api_key], CONFIG[:api_secret]
    opentok #TODO: usunac to
  end
  
  def find_tutor(tutor_id)
    Tutor.find(tutor_id)    
  end
  
  def find_user(user_id)
    User.find(user_id) 
  end
  
  def signed_in
    unless user_signed_in?
      flash[:notice] = "Aby kontynuować musisz się zalogować."
      redirect_to new_user_session_path
    end
  end
  
  def assign_access(tutor, learner)
    unless ( current_user == tutor ) or (current_user == learner)  
      flash[:notice] = "Korepetytor aktualnie nie wyznaczył Ciebie jako ucznia. Dostęp do prywatnej lekcji zablokowany."
      redirect_to root_path
    end
  end

  #jezeli ktos bawi sie linkami
  def protect_from_hack(user)
    unless current_user == user
      redirect_to root_path
    end
  end
  
end
