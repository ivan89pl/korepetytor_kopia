# encoding: utf-8
# #!/bin/env ruby
class CommentsController < ApplicationController
  before_filter :access_control, :only => [ :edit, :update ]
  def edit
    @comment = Comment.find_by_id(params[:id])
    @tutor = @comment.tutor
    @comment.update_attributes(params[:comment])
  end
  
  def update
    @comment = Comment.find_by_id(params[:id])
    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        @comment.tutor.new_comment_handler!(@comment.rate)
        flash[:success] = "Pomyślnie dodano komentarz."
        format.html { redirect_to user_comments_path(current_user) }
      else
        format.html { render :action => "edit" } 
      end
    end
  end

  def index
    
    if params[:user_id].present?
      access_control
      @user = User.find_by_id(params[:user_id])      
      @comments = Comment.unpublished(@user.id)
      
    elsif params[:tutor_id].present?
      @tutor = Tutor.find_by_id(params[:tutor_id])
      @comments = @tutor.comments.published.includes(:user)
    end

  end
  
  private
  

  
end
