# encoding: utf-8
# #!/bin/env ruby
class ContactsController < ApplicationController
  
  def create
    @message = MailMessage.new(params[:mail_message])
    @message.name = current_user.nickname
    @message.email = current_user.email
    
    if @message.valid?
      NotificationsMailer.new_message(@message).deliver
      redirect_to(user_payment_management_path(current_user), :notice => "Wiadomość została wysłana.")
    else
      flash.now.alert = "Proszę wypełnić wszystkie pola."
      redirect_to user_payment_management_path(current_user)
    end
  end

end
#:name, :email, :subject, :body, :tokens_amount