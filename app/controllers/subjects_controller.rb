class SubjectsController < ApplicationController
  def index
    @subjects = Subject.all
    @online_tutors = Tutor.only_online.includes(:user)
    @offline_tutors = Tutor.only_offline.includes(:user).limit(8)
  end

  def show
    @subject = find_subject(params[:subject_id])
  end
  
  private
  
  def find_subject(subject_id)
    Subject.find_by_id(subject_id)
  end
  
end
