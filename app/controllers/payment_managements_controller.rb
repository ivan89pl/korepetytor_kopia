# encoding: utf-8
# #!/bin/env ruby
class PaymentManagementsController < ApplicationController
  before_filter :access_control

  def show

    @user = find_user(params[:user_id])
  end  
  
  private
  
  def find_user(user_id)
    User.find_by_id(user_id)
  end

end
