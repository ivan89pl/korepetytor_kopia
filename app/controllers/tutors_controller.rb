# encoding: utf-8
# #!/bin/env ruby
class TutorsController < ApplicationController
  before_filter :signed_in, :except => [ :show, :index ]
 
  
  def index
    @subject = Subject.find_by_id(params[:subject_id]) 
    online_tutors = @subject.tutors.only_online.includes(:user)
    offline_tutors = @subject.tutors.only_offline.includes(:user)
    all_tutors = online_tutors + offline_tutors
    @tutors =  all_tutors.paginate(:page => params[:page], :per_page => CONST[:per_page])
  end
  
  
  def show
    user = find_user(params[:user_id])
    @tutor = user.tutor
    @subjects = find_subjects(@tutor)
  end
  
  def new
    user = find_user(params[:user_id])
    @tutor = user.build_tutor
    build_subject(@tutor)
    authorize!(:new, @tutor)   
  end
  
  def create
    user = find_user(params[:user_id])  
    @tutor = user.build_tutor(params[:tutor])
   #TODO: dodac ustawianie last_seen_at
    respond_to do |format|
      if @tutor.save
        format.html { redirect_to new_user_room_path(user) }
      else
        format.html { redirect_to new_user_tutor_path(user) } 
      end
    end
  end
  
  def edit
    user = find_user(params[:user_id])
    @tutor = user.tutor
    authorize!(:manage, @tutor)
    build_subject(@tutor) unless @tutor.subjects.present?
  end
  
  def update
    user = find_user(params[:user_id])
    @tutor = user.tutor
    authorize!(:manage, @tutor)
    respond_to do |format|
      if @tutor.update_attributes(params[:tutor])
        format.html { redirect_to user_tutor_path(user) }
      else
        format.html { render :action => "edit" } 
      end
    end
  end
  
  def online
    tutor = find_tutor(params[:id])
    tutor.set_online_status!
    render :nothing => true
  end
  
  
  
  private 
  
  def find_tutor(id)
    Tutor.find_by_id(id)
  end
  
  def find_user(user_id)
    User.find(user_id)
  end
  
  def find_subjects(tutor)
    tutor.subjects
  end
  
  def build_subject(tutor)
    tutor.subjects.build
    tutor.tutor_subjects.build
  end
  
  def signed_in
    unless user_signed_in?
      flash[:notice] = "Aby kontynuować musisz się zalogować."
      redirect_to new_user_session_path
    end
  end
  
end
