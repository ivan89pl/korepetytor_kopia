# encoding: utf-8
# #!/bin/env ruby
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
  
  def access_control
    #tylko dla wlasciciela
    if current_user == User.find_by_id(params[:user_id]) and user_signed_in?
      return
    #gdy uzytkownik jest niezalogowany
    elsif !user_signed_in?
      flash[:notice] = "Aby kontynuować musisz się zalogować."
      redirect_to new_user_session_path  
    #w kazdym innym przypadku 
    else
      flash[:error] = "Nie masz praw do przeglądania profili innych użytkowników."
      redirect_to root_path 
    end
  end

  #kasuje pokoje ktore wygasly, zwraca liste aktywnych pokoi
  def destroy_expired_room(private_rooms)
    container = []  
    private_rooms.each do |room|
      room.expired? ? room.delete : container << room
    end
    return container
  end


end
